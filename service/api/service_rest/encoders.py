from black import Mode
from common.json import ModelEncoder

from .models import (
    AutomobileVO,
    Technician, 
    Service,
)


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number"
    ]


class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "id",
        "vin",
        "customer_name",
        "appointment",
        "technician",
        "reason",
        "is_completed",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }