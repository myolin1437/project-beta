from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    TechnicianEncoder,
    ServiceEncoder,
    AutomobileVOEncoder,
)
from .models import (
    AutomobileVO,
    Technician,
    Service,
)


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_services(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {"services": services},
            encoder=ServiceEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician_id"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
            service = Service.objects.create(**content)
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the service"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET"])
def api_automobiles_vo(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
        )


@require_http_methods(["DELETE", "PUT"])
def api_service(request, pk):
    if request.method == "DELETE":
        try:
            service = Service.objects.get(id=pk)
            service.delete()
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )   
        except Service.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            if "technician_id" in content:
                technician = Technician.objects.get(id=content["technician_id"])
                content["technician"] = technician
            Service.objects.filter(id=pk).update(**content)
            service = Service.objects.get(id=pk)
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response