from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name


class Service(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=100)
    appointment = models.DateTimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.PROTECT,
    )
    reason = models.TextField()
    is_completed = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_service", kwargs={"pk": self.pk})

    def __str__(self):
        return f'{self.customer_name}-{self.vin}'