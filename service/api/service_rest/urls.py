from django.urls import path

from .views import (
    api_technicians,
    api_services,
    api_automobiles_vo,
    api_service,
)


urlpatterns = [
    path(
        "technicians/",
        api_technicians,
        name="api_technicians",
    ),
    path(
        "services/",
        api_services,
        name="api_services",
    ),
    path(
        "automobiles_vo/",
        api_automobiles_vo,
        name="api_automobiles_vo",
    ),
    path(
        "services/<int:pk>/", 
        api_service, 
        name="delete_service",
    ),
]