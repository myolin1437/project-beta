# Generated by Django 4.0.3 on 2022-08-03 02:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0007_alter_service_appointment_time'),
    ]

    operations = [
        migrations.RenameField(
            model_name='service',
            old_name='vin',
            new_name='automobile',
        ),
    ]
