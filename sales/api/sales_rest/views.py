from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import SalesPerson, Customer, SaleRecord, AutomobileVO
import json
# Create your views here.


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "name", "employee_number"]


class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "name", "employee_number"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "name", "address", "phone_number"]

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "import_href", "vin"]

class SaleRecordDetailEncoder(ModelEncoder):
    model = SaleRecord
    properties = ["id", "automobile", "sales_person", "customer", "price"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "sales_person": SalesPersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person, 
                encoder=SalesPersonDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create sales person"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer, 
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_sale_record(request):
    if request.method == "GET":
        sale_record = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_record": sale_record},
            encoder=SaleRecordDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            auto_id = content["auto_id"]
            auto = AutomobileVO.objects.get(id=auto_id)
            content["automobile"] = auto
            
            sales_person_id = content["sales_person_id"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person

            customer_id = content["customer_id"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            del content["auto_id"]
            del content["sales_person_id"]
            del content["customer_id"]
            sale_record = SaleRecord.objects.create(**content)
            return JsonResponse(
                sale_record,
                encoder=SaleRecordDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create sale record"}
            )
            response.status_code = 400
            return response
    
    
    
@require_http_methods("GET")
def api_get_automobilevo(request):
    if request.method == "GET":
        automobile_vo = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobile_vo": automobile_vo},
            encoder=AutomobileVODetailEncoder,
        )

@require_http_methods("DELETE")
def api_delete_automobilevo(request, vin):
    if request.method == "DELETE":
        try:
            auto = AutomobileVO.objects.get(vin=vin)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=AutomobileVODetailEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})