from django.db import models


# Create your models here.
class SalesPerson(models.Model):
    name = models.CharField(max_length=75)
    employee_number = models.PositiveIntegerField(unique=True)


class Customer(models.Model):
    name = models.CharField(max_length=75)
    address = models.CharField(max_length=200)
    phone_number = models.TextField(max_length=15)



class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    


class SaleRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale_record",
        on_delete=models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sale_record",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sale_record",
        on_delete=models.CASCADE,
    )
    price = models.PositiveIntegerField()