from django.urls import path

from .views import api_list_sales_person, api_list_customer, api_list_sale_record, api_get_automobilevo, api_delete_automobilevo 

urlpatterns = [
    path("sales_person/", api_list_sales_person, name="api_create_sales_person"),
    path("customer/", api_list_customer, name="api_create_customer"),
    path("sale_record/", api_list_sale_record, name="api_create_sale_record"),
    path("automobile_vo/", api_get_automobilevo, name="api_get_automobile"),
    path(
        "automobile_vo/<str:vin>/",
        api_delete_automobilevo,
        name="api_delete_automobile",
    ),
]