# CarCar

Team:

* Myo - Automobile Service; Manufacturer, Vehical Model
* Jack - Auto Sales; Automobile

## Commands to run the application

    - Open docker desktop
    - In the terminal at the project directory, run the following commands:
        - docker volume create beta-data
        - docker compose build
        - docker compose up

## Design

Bounded Context:
    There are three bounded context within the domain: 
        - Inventory (three entities: Manufacturer, Vehical Model, and Automobile)
        - Automobile Service (three entities: AutomobileVO, Service, Technician)
        - Auto Sales (four entities: Sales Person, Customer, AutomobileVO, Sale Record)

Navigations design:
    - The navigation links to view a list of manufacturers, vehicle models, automobiles, service appointments, sales records, sales person history and to create a technician, customer, sales person are located in the navigation bar at the top of the webpage.
    - The navigation links to create a manufacturer, vehicle model, automobile, service appointment, sales record are located in each respective component list page.

For each other the component in the Inventory, there is a page to show a list of that specific component and their information, and a page to fill out the form and create that specific component to be added to the list.

For the Automobile Service component, there are 2 pages: 
    1. The page to fill out the form to create a service appointment
    2. The page to shows a list of both current (pending) and past (completed) appointments and their information. This page will have a search feature to look up a specific vin (either current or past appointment). There are 3 buttons (Cancel, Finished, Undo) for each of the service appointment listed. 
        - Cancel button: on click, deletes the appointment from the list as well as from the database.
        - Finished button: on click, changes the status of the appointment from pending to completed, show the undo button, and disable the cancel button.
        - Undo button: on click, changes the status of the appointment back to pending, show the finished button, and enable the cancel button.

For the Sale Record componenet, there are three pages:
    1. A sale record form to fill out a service record form with the automobile VIN number, sale person, customer, and price
    2. a list of all sales including the information listed from the sale record
    3. a separate list that can filter out the sales from a specific salesman and only their sales with the information from the sales record form

## Service microservice

Explain your models and integration with the inventory
microservice, here.

For the service microservice, I'm expecting to have 3 models (Service, Technician, and AutomobileVO). 

The Service model would be the aggregate root to the Technician model. 

And the AutomobileV0 data would be mapped through polling from the Automobile model in the poller file with possibly only the VIN information.

The AutomobileV0 model will be used in the front-end to compare whether a automobile making a service appoinment was brought from the dealership or not in order to identify VIP customers.

In order to create a form to enter a technician information, I would create a Technician model with 2 attributes (name Entity and employee number Value Object) and then create an api view function to get the list of the technicians, then create the api url path to that list and connects that to the url file in the project directory.

For the Service model, I would need to have attributes for automobile, customer name, date, time, technician, and reason.

For the AutomobileVO model, I would need only one attribute for the VIN number.

## Sales microservice

For models, I will create four models with two their own entities (sales person and customer), a model that polls the necessary data from the automobile model in inventory (Automobile VO), and one model (sale record) that requires data from the previous thre models while having price its own seperate entity.

for sales person model, I only need to provide the name and employee number as the attributes. I will have employee number as a positive integer field as I don't know how long an employee number will be, but for the testing I will only use four numbers.

for customer, I need the attributes of name, address, and phone number. I will make the first two charfields, but for phone number I am going to use textfield as you need to input dashes inbetween the numbers and the positive integer field can't do that.  

 I only need one model to integrate with the inventory microservice and ignore the rest as the automobile model already contains the information needed from the Vehicle model and manufacturer models. for that I will create Automobile VO, and though the automobile has inforation from other models in inventory, I will only need the href and vin numbers to complete what I need for the sale record model.

 for the sale record model, I will use foreign keys to access the models mentioned earlier to access the necessary data when creating the sale record form and list, but for the price attribute of the model I will use a positive integer field.
 
 In hindsight: The only issue that my model has is that when entering the price for the sale record, it does not take commas as I thought it would be best to use a positive integer field to enter the price number. In hindisght, I would maybe do the price section of the model not be positive integer field to allow commas in the price.  