import React from "react";
import { Link } from 'react-router-dom';


class SaleRecordList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {saleRecords: []}
    }
    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/sale_record/');
        if (response.ok) {
            const data = await response.json();
            this.setState({saleRecords : data.sale_record});
            
        }
    }
    render() {
        return (
            <>
                <h1>Sale Records</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.saleRecords.map(saleRecord => {
                            return (
                                <tr key={saleRecord.id}>
                                    <td>{ saleRecord.sales_person.name }</td>
                                    <td>{ saleRecord.customer.name }</td>
                                    <td>{ saleRecord.automobile.vin }</td>
                                    <td>${ saleRecord.price }</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <Link to="/sale_record/new">
                    <button className="btn btn-outline-success btn-sm">
                        Add a sale record
                    </button>
                </Link>
            </>
        )
    }
}

export default SaleRecordList;