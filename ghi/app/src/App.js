import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';
import ModelsList from './ModelsList';
import ModelForm from './ModelForm';
import TechnicianForm from './TechnicianForm';
import ServiceApptsList from './ServiceApptsList';
import ServiceForm from './ServiceForm';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SaleRecordForm from './SaleRecordForm';
import SaleRecordList from './SaleRecordList';
import SalesPersonHistoryList from './SalesPersonHistoryList';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="" element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobilesList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelsList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="services">
            <Route path="" element={<ServiceApptsList />} />
            <Route path="new" element={<ServiceForm />} />
          </Route>
          <Route path="sales_person">
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
          <Route path="customer">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sale_record">
            <Route path="" element={<SaleRecordList />} />
            <Route path="new" element={<SaleRecordForm />} />
            <Route path="history" element={<SalesPersonHistoryList />} />
          </Route>
        </Routes> 
      </div>
    </BrowserRouter>
  );
}

export default App;
