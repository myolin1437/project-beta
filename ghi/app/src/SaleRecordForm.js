import React from "react";

class SaleRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobiles: [],
            salesPersons: [],
            customers: [],
            price: '',
        };
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }  

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.salesPersons;
        delete data.automobiles;
        delete data.customers;
        data.auto_id = data.automobile
        data.sales_person_id = data.sales_person
        data.customer_id = data.customer
        delete data.automobile
        delete data.sales_person
        delete data.customer
        console.log(data)
        
        const saleRecordUrl = "http://localhost:8090/api/sale_record/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(saleRecordUrl, fetchConfig);
        if (response.ok) {
            this.setState({
                automobile: '',
                sales_person: '',
                customer: '',
                price: '',
              });
        }
    }
    
    async handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({automobile: value})
      }

    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({sales_person: value})
      }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({customer: value})
        }
      
    handlePriceChange(event) {
        const value = event.target.value;
        this.setState({price: value})
      }


    async componentDidMount() {

        const autoResponse = await fetch('http://localhost:8090/api/automobile_vo/');
        const salesPersonResponse = await fetch('http://localhost:8090/api/sales_person/');
        const customerResponse = await fetch('http://localhost:8090/api/customer/');
        const saleRecordResponse = await fetch("http://localhost:8090/api/sale_record/")

        if (autoResponse.ok && salesPersonResponse.ok && customerResponse.ok && saleRecordResponse.ok) {
          const autoData = await autoResponse.json();
          const salesData = await salesPersonResponse.json();
          const customerData = await customerResponse.json();
          const saleRecordData = await saleRecordResponse.json();
          this.setState({
              automobiles: autoData.automobile_vo,
              salesPersons: salesData.sales_persons,
              customers: customerData.customers,
            });
        const autoVinList = []  
        for (let auto of autoData.automobile_vo) {
            autoVinList.push(auto.vin)
          } 
        const soldVinList = [];
        for (let sale of saleRecordData.sale_record) {
            soldVinList.push(sale.automobile.vin);
        }
        const unsoldAutoList = []
        for (let vin of autoVinList) {
            if (!soldVinList.includes(vin)){
            unsoldAutoList.push(vin)
            }
        }
        const availableList = []
        for (let auto of autoData.automobile_vo) {
            if (unsoldAutoList.includes(auto.vin)) {
                availableList.push(auto)
            }
        }
        this.setState({automobiles: availableList})
    }
      }

    render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create sale record</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                  <div className="mb-3">
                    <select value={this.state.automobile} onChange={this.handleAutomobileChange} required name="automobile" id="automobile" className="form-select">
                      <option value="">Automobile</option>
                      {this.state.automobiles.map(automobile => {
                            return (
                                <option key={automobile.import_href} value={automobile.id}>
                                    {automobile.vin}
                                </option>
                            );
                        })}
                    </select>
                  </div>
                  <div className="mb-3">
                    <select value={this.state.sales_person} onChange={this.handleSalesPersonChange} required name="sales_person" id="sales_person" className="form-select">
                      <option value="">Sales person</option>
                      {this.state.salesPersons.map(salesPerson => {
                            return (
                            <option key={salesPerson.id} value={salesPerson.id}>
                                {salesPerson.name}
                            </option>
                            );
                        })}
                    </select>
                  </div>
                  <div className="mb-3">
                    <select value={this.state.customer} onChange={this.handleCustomerChange} required name="customer" id="customer" className="form-select">
                      <option value="">Customer</option>
                      {this.state.customers.map(customer => {
                            return (
                            <option key={customer.id} value={customer.id}>
                                {customer.name}
                            </option>
                            );
                        })}
                    </select>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.price} onChange={this.handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control"/>
                    <label htmlFor="price">Price</label>
                  </div>
                  <button className="btn btn-outline-success">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    
}

export default SaleRecordForm;