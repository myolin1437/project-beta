import React from "react";
import { Link } from 'react-router-dom';


class ModelsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {models: []}
    }
    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            this.setState({models : data.models})
        }
    }
    render() {
        return (
            <>
                <h1>Vehicle models</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => {
                            return (
                                <tr key={model.href}>
                                    <td>{ model.name }</td>
                                    <td>{ model.manufacturer.name }</td>
                                    <td>
                                        <img src={ model.picture_url } alt="" width="200" />
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <Link to="/models/new">
                    <button className="btn btn-outline-success btn-sm">
                        Add a vehicle model
                    </button>
                </Link>
            </>
        )
    }
}

export default ModelsList;