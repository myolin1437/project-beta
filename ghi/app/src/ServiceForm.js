import React, { useState, useEffect, useRef } from "react"
import { useNavigate } from "react-router-dom"


function ServiceForm(props) {
  const [technicians, setTechnicians] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    (async () => {
        const techniciansResponse = await fetch('http://localhost:8080/api/technicians/');
        if (techniciansResponse.ok) {
            const techniciansData = await techniciansResponse.json();
            setTechnicians(techniciansData.technicians)
        }
    })()
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      vin: event.target.vin.value,
      customer_name: event.target.customer_name.value,
      appointment: event.target.appointment.value,
      technician_id: event.target.technician_id.value,
      reason: event.target.reason.value,
    }
    
    const serviceUrl = 'http://localhost:8080/api/services/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(serviceUrl, fetchConfig);
    if (response.ok) {
      navigate('/services')
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Enter a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-service-form">
            <div className="form-floating mb-3">
              <input placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Customer name" required type="text" name="customer_name" id="customer_name" className="form-control"/>
              <label htmlFor="customer_name">Customer name</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Appointment" required type="datetime-local" name="appointment" id="appointment" className="form-control"/>
              <label htmlFor="appointment">Appointment</label>
            </div>
            <div className="mb-3">
              <select required name="technician_id" id="technician_id" className="form-select">
                <option value="">Choose a technician</option>
                  {technicians.map(technician => {
                        return (
                          <option key={technician.id} value={technician.id}>
                              { technician.name } - { technician.employee_number } 
                          </option>
                        );
                    })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-outline-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ServiceForm;