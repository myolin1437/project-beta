function TechnicianForm(props) {

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      name: event.target.name.value,
      employee_number: event.target.employee_number.value,
    }
    
    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
        event.target.name.value = '';
        event.target.employee_number.value = '';
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Enter a technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Employee number" required type="number" name="employee_number" id="employee_number" className="form-control"/>
              <label htmlFor="employee_number">Employee number</label>
            </div>
            <button className="btn btn-outline-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;