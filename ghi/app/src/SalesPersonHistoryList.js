import React from "react";

class SalesPersonHistoryList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {salesPersons: [], saleRecords: []};
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
    } 




    async handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({sales_person: value})
        const response = await fetch('http://localhost:8090/api/sale_record/');
        if (response.ok){
            const data = await response.json();
            const updatedList = data.sale_record.filter(saleRecord => {return saleRecord.sales_person.name == event.target.value});
            if (value == "") {
                this.setState({saleRecords : data.sale_record}); 
            }
            else { 
                this.setState({saleRecords : updatedList});
            } 
        }
      }

    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/sale_record/');
        const personResponse = await fetch('http://localhost:8090/api/sales_person/')
        if (response.ok && personResponse.ok) {
            const data = await response.json();
            const personData = await personResponse.json()
            this.setState({saleRecords : data.sale_record});
            this.setState({salesPersons : personData.sales_persons})
        }
    }
    
    render() {
        return (
            <>
                <h1>Sales Person History</h1>
                <div className="mb-3">
                    <select value={this.state.sales_person} onChange={this.handleSalesPersonChange} required name="sales_person" id="sales_person" className="form-select">
                      <option value="">Sales person</option>
                      {this.state.salesPersons.map(salesPerson => {
                            return (
                            <option value={salesPerson.name} key={salesPerson.id}>
                                {salesPerson.name}
                            </option>
                            );
                        })}
                    </select>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.saleRecords.map(saleRecord => {
                            return (
                                <tr key={saleRecord.id}>
                                    <td>{ saleRecord.sales_person.name }</td>
                                    <td>{ saleRecord.customer.name }</td>
                                    <td>{ saleRecord.automobile.vin }</td>
                                    <td>${ saleRecord.price }</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        )
    }
}

export default SalesPersonHistoryList;