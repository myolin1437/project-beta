import React, { useState, useEffect, useRef } from "react"
import { Link } from 'react-router-dom';


function ServiceApptsList(props) {
    const [services, setServices] = useState([]);
    const [filteredServices, setFilteredServices] = useState([]);
    const [vips, setVips] = useState([]);
    const [vipVins, setVipVins] = useState([]);


    async function getServices() {
        try {
            const servicesResponse = await fetch('http://localhost:8080/api/services/');
            if (servicesResponse.ok) {
                const servicesData = await servicesResponse.json();
                setServices(servicesData.services);
            }
            return servicesResponse;
        } catch(error) {
            console.log(error);
        }
    }

    async function getVips() {
        try {
            const vipsResponse = await fetch('http://localhost:8080/api/automobiles_vo/');
            if (vipsResponse.ok) {
                const vipsData = await vipsResponse.json();
                setVips(vipsData.automobiles);
            }
        } catch(error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getServices();
        getVips();
    }, [])
    
    useEffect(() => {
        (() => {
            setVipVins(vips.map(vip => vip.vin));
        })()
    }, [vips])

    function handleChange() {
        setFilteredServices(services);
    }

    useEffect(() => {handleChange()}, [services])
    
    function handleSubmit(event) {
        event.preventDefault();
        const vin = event.target.vin.value;
        setFilteredServices(services.filter(service => service.vin === vin));
    }

    async function handleDelete(service) {
        const response = await fetch(`http://localhost:8080/api/services/${service.id}/`, {method: "delete"});
        if (response.ok) {
            const idx = services.indexOf(service);
            const updatedServices = [...services];
            updatedServices.splice(idx, 1);
            setServices(updatedServices);
        }
    }

    async function handleCompleteOrUndo(service) {
        const data = service.is_completed ? {is_completed: false} : {is_completed: true};
        const url = `http://localhost:8080/api/services/${service.id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const completedService = await response.json()
            const idx = services.indexOf(service);
            const updatedServices = [...services];
            updatedServices.splice(idx, 1, completedService);
            setServices(updatedServices);
        }
    }

    function checkIfComplete(service) {
        if (service.is_completed) {
            return (
                <>
                    <td>Completed</td>
                    <td>
                        <button className="btn btn-outline-danger me-2" disabled>Cancel</button>
                        <button onClick={() => handleCompleteOrUndo(service)} className="btn btn-outline-secondary me-2">Undo</button>
                    </td>
                </>
            )
        } else {
            return (
                <>
                    <td>Pending</td>
                    <td>
                        <button onClick={() => handleDelete(service)} className="btn btn-outline-danger me-2">Cancel</button>
                        <button onClick={() => handleCompleteOrUndo(service)} className="btn btn-outline-success me-2">Finished</button>
                    </td>
                </>
            )
            
        }
    }

    function appointmentsList() {
        if (filteredServices.length === 0) {
            return (
                <tr>
                    <td colSpan={8}>No appointments found</td>
                </tr>
            )    
        } else {
            return (
                filteredServices.map(service => {
                    return (
                        <tr key={service.id}>
                            <td>{ service.vin }</td>
                            <td>
                                { vipVins.includes(service.vin) ? `${service.customer_name} (VIP)` : service.customer_name }
                            </td>
                            <td>{ new Date(service.appointment).toLocaleDateString() }</td>
                            <td>{ new Date (service.appointment).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit'}) }</td>
                            <td>{ service.technician.name }</td>
                            <td>{ service.reason }</td>
                                { checkIfComplete(service) }
                        </tr>
                    )
                })
            )}
        }

    return (
        <>
            <form onSubmit={handleSubmit} onChange={handleChange} className="d-flex mt-3 mb-3" >
                <input placeholder="Search VIN" type="search" name="vin" id="vin" aria-label="Search" className="form-control me-2"/>
                <button className="btn btn-outline-secondary text-nowrap me-2" type="submit">Search VIN</button>
            </form>
            <h1>Service appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th colSpan={2}>Status</th>
                    </tr>
                </thead>
                <tbody>
                    { appointmentsList() }
                </tbody>
            </table>
            <Link to="/services/new">
                <button className="btn btn-outline-success btn-sm">
                    Enter a service appointment
                </button>
            </Link>
        </>
    )
}

export default ServiceApptsList;